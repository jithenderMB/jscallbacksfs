const fs = require('fs');
const path = require('path');

const getFsOperations = (fileName) => {

    fs.mkdir(path.resolve(__dirname, fileName), (err) => {

        if (err) {
            console.log(err);
        }
        else {
            console.log("Folder created successfully")
        }

    })

    return {

        readFolder: (path, cb) => {

            fs.readdir(path, 'utf-8', (err, data) => {
                if (err) {
                    console.log(err);
                }
                else {
                    cb(data);
                }
            });
        },

        createFile: (path, data, cb) => {

            fs.writeFile(path, data, (err) => {

                if (err) {
                    console.log(err);
                }
                else {
                    console.log("File created successfully")
                    cb();
                }
            });
        },

        deleteFile: (path) => {

            fs.unlink(path, (err) => {

                if (err) {
                    console.log(err);
                }
                else {
                    console.log("File Deleted successfully")
                }
            });
        }

    }
}

module.exports = getFsOperations;