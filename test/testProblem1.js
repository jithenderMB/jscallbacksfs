const path = require('path');

const getFsOperations = require("../problem1");

let fsOperations = getFsOperations('JsonFilesFolder');

let folderPath = '../JsonFilesFolder';

let file1 = 'file1.json';
let file2 = 'file2.json';
let file3 = 'file3.json';
let file4 = 'file4.json';


fsOperations.createFile(path.resolve(folderPath,file1), 'JSON file-1 created', () => {

    fsOperations.createFile(path.resolve(folderPath,file2), 'JSON file-2 created', () => {

        fsOperations.createFile(path.resolve(folderPath,file3), 'JSON file-3 created', () => {

            fsOperations.createFile(path.resolve(folderPath,file4), 'JSON file-4 created', () => {

                fsOperations.readFolder(folderPath, (data) => {

                    data.forEach(element => {
                        fsOperations.deleteFile(path.resolve(folderPath, element));
                    })
                })
            })
        })
    })
})








