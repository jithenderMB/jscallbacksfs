const fs = require('fs');

const path = require('path');

let fsOperations = () => {
    return {
        createFolder: (path) => {
            fs.mkdir(path, (err) => {
                if (err) {
                    console.log(err);
                }
            })
        },

        readTheFile: (path, cb) => {
            fs.readFile(path, 'utf-8', (err, data) => {
                if (err) {
                    console.log(err);
                }
                else {
                    cb(data);
                }
            })
        },

        writeInTheFile: (path, data, cb) => {
            fs.writeFile(path, data, { encoding: 'utf-8', flag: 'a' }, (err) => {
                if (err) {
                    console.log(err);
                }
                else {
                    cb();
                }
            })
        },

        removeTheFile: (path) => {
            fs.unlink(path, (err) => {
                if (err) {
                    console.log(err);
                }
            })
        }
    }
}


let myWork = fsOperations();

let userDataPath = './lipsum.txt';

let conertToUppercaseFileName = 'convertToUpperCase.txt';
let conertToUppercaseFilePath = './convertToUpperCase.txt';

let conertToLowercaseFileName = 'convertToLowerCase.txt';
let conertToLowercaseFilePath = './convertToLowerCase.txt';

let sortTheContentFileName = 'sortTheContent.txt';
let sortTheContentFilePath = './sortTheContent.txt';

let filesNamesFilePAth = './filenames.txt';



myWork.readTheFile(userDataPath, (data) => {

    myWork.writeInTheFile(conertToUppercaseFilePath, data.toUpperCase(), () => {

        myWork.writeInTheFile(filesNamesFilePAth, conertToUppercaseFileName + '\n', () => {

            myWork.readTheFile(conertToUppercaseFilePath, (data) => {

                myWork.writeInTheFile(conertToLowercaseFilePath, JSON.stringify(data.toLowerCase().split('. ')), () => {

                    myWork.writeInTheFile(filesNamesFilePAth, conertToLowercaseFileName + '\n', () => {

                        myWork.readTheFile(conertToLowercaseFilePath, (data) => {
                            let requiredData = JSON.parse(data).sort((a, b) => {
                                return a.length - b.length;
                            })
                            myWork.writeInTheFile(sortTheContentFilePath, JSON.stringify(requiredData), () => {

                                myWork.writeInTheFile(filesNamesFilePAth, sortTheContentFileName + '\n', () => {

                                    myWork.readTheFile(filesNamesFilePAth, (data) => {

                                        data.split('\n').forEach(file => {
                                            if (file != '') {
                                                myWork.removeTheFile(path.resolve(__dirname, file));
                                            }

                                        })
                                    })
                                });
                            })

                        })

                    })
                })

            })
        })
    })
});

